# micro

[![Build Status](https://travis-ci.org/nzwsch/micro.svg?branch=master)](https://travis-ci.org/nzwsch/micro)

Simple microblogging platform.

## Screenshot

![Screenshot](screenshot.jpg)

[Running Demo](https://micro.seiichiyonezawa.com)

### Ruby version

Currently we use `2.4.4`.

### System dependencies

Tested `PostgreSQL 10.5 (Ubuntu 10.5-0ubuntu0.18.04)`.

#### How to run the test suite

    $ rake db:create
    $ rake yarn:install
    $ rake db:migrate
    $ rake test

### Services (job queues, cache servers, search engines, etc.)

This app is hosted on [Heroku](https://heroku.com).

### Deployment instructions

    $ heroku create
    $ git push heroku master
    $ heroku run rake db:migrate
    $ heroku run rails console
    >> Account.create({ name: 'YOUR_NAME', email: 'YOUR_EMAIL', password: 'PASSWORD', avatar_url 'https://www.gravatar.com/avatar/0' })
    >> exit
    $ heroku open
