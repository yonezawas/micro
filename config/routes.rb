Rails.application.routes.draw do
  devise_for :accounts
  resources :microposts
  root 'microposts#index'
end
