require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @micropost = microposts(:one)
    sign_in @micropost.account
  end

  test "should get index" do
    get microposts_url
    assert_response :success
  end

  test "should get new" do
    get new_micropost_url
    assert_response :success
  end

  test "should create micropost" do
    assert_difference('Micropost.count') do
      post microposts_url, params: { micropost: { body: @micropost.body } }
    end

    assert_redirected_to root_url
  end

  test "should create micropost and card" do
    card_attributes = {
      url: 'http://example.com',
      title: 'Sample Card',
      description: 'Card Description',
      image: 'http://example.com/image.jpg'
    }

    assert_difference('Card.count') do
      post microposts_url, params: { micropost: { body: @micropost.body, card_attributes: card_attributes } }
    end
  end

  test "should not create blank card" do
    card_attributes = {
      url: '',
      title: '',
      description: '',
      image: ''
    }

    assert_no_difference('Card.count') do
      post microposts_url, params: { micropost: { body: @micropost.body, card_attributes: card_attributes } }
    end
  end

  test "should show micropost" do
    get micropost_url(@micropost)
    assert_response :success
  end

  test "should get edit" do
    get edit_micropost_url(@micropost)
    assert_response :success
  end

  test "should update micropost" do
    patch micropost_url(@micropost), params: { micropost: { body: @micropost.body } }
    assert_redirected_to micropost_url(@micropost)
  end

  test "should destroy micropost" do
    assert_difference('Micropost.count', -1) do
      delete micropost_url(@micropost)
    end

    assert_redirected_to microposts_url
  end
end
