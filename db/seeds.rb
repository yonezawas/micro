# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
account = Account.create!({
  avatar_url: 'https://avatars3.githubusercontent.com/u/19492382?s=128&v=4',
  email: 'admin@example.com',
  name: 'Seiichi Yonezawa',
  password: 'password',
})

30.times do
  micropost = Micropost.create!({
    account_id: account.id,
    body: 'Hello, world!',
  })
end if Rails.env.development?
