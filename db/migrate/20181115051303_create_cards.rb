class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.references :micropost, foreign_key: true
      t.string :url
      t.string :title
      t.string :description
      t.string :image

      t.timestamps
    end
  end
end
