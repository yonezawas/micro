class CreateMicroposts < ActiveRecord::Migration[5.2]
  def change
    create_table :microposts do |t|
      t.references :account, foreign_key: true
      t.text :body

      t.timestamps
    end
  end
end
