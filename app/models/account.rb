class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :registerable, :recoverable, :timeoutable, :lockable and :omniauthable
  devise :database_authenticatable, :rememberable, :validatable, :trackable
  has_many :microposts
end
