class Card < ApplicationRecord
  belongs_to :micropost
  validates_presence_of :url
end
