class Micropost < ApplicationRecord
  belongs_to :account
  has_one :card, dependent: :destroy

  accepts_nested_attributes_for :card, reject_if: :all_blank
end
