# frozen_string_literal: true

module MicropostsHelper
  LINK_REL = 'nofollow noopener noreferrer'

  def sanitize_and_replace_to_link(body)
    sanitize(strip_links(body).gsub(%r{(https?://[^\s]+)}) do |link|
      text = truncate(link.sub(%r{https?://}, ''))
      link_to(text, link, rel: LINK_REL, target: '_blank')
    end, tags: %w[a], attributes: %w[href rel target])
  end
end
