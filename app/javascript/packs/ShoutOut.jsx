import React, { Component } from "react";
import PropTypes from "prop-types";
import ShoutForm from "../components/ShoutForm";

export const HTTP_PATTERN = /^(https?:\/\/[^\s]+)\s+/;

class ShoutOut extends Component {
  state = {
    body: "",
    card: "",
    limitBody: 160
  };

  findURL = string => {
    const match = string.match(HTTP_PATTERN);
    return match ? match[1] : "";
  };

  changeBody = event => {
    const body = event.target.value.replace("\n", "");
    const card = this.findURL(body);

    this.setState({ card, body });
  };

  render() {
    return (
      <ShoutForm
        action={this.props.action}
        token={this.props.token}
        card={this.state.card}
        body={this.state.body}
        limitBody={this.state.limitBody}
        changeBody={this.changeBody}
        beforeSubmit={this.beforeSubmit}
      />
    );
  }
}

ShoutOut.propTypes = {
  action: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired
};

export default ShoutOut;
