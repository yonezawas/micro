/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb
import Rails from "rails-ujs";
import React from "react";
import ReactDOM from "react-dom";
import ShoutOut from "./ShoutOut";

Rails.start();

document.addEventListener("DOMContentLoaded", () => {
  const div = document.getElementById("shout_out");

  if (div != null) {
    const meta = document.querySelector("meta[name=csrf-token]");
    const token = meta.getAttribute("content");
    ReactDOM.render(<ShoutOut action="/microposts" token={token} />, div);
  }
});
