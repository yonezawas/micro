import React from "react";
import PropTypes from "prop-types";

import { HTTP_PATTERN } from "../packs/ShoutOut";

const ShoutForm = props => {
  const isShoutable = props.body.length > props.limitBody;
  const limitClass = isShoutable ? "my-0 text-danger" : "my-0 text-secondary";
  const bodyLength = props.body.replace(HTTP_PATTERN, "").length;
  const limitCount = props.limitBody - bodyLength;

  return (
    <form action={props.action} method="post" onSubmit={props.beforeSubmit}>
      <input type="hidden" name="authenticity_token" value={props.token} />
      <div className="form-group row">
        <div className="col">
          <h2 className="my-0 text-primary">What's Up?</h2>
        </div>
        <div className="col text-right">
          <h2 className={limitClass}>{limitCount}</h2>
        </div>
      </div>
      <div className="form-group">
        <input
          type="hidden"
          name="micropost[card_attributes][url]"
          value={props.card}
        />
        <textarea
          className="form-control"
          name="micropost[body]"
          value={props.body}
          onChange={props.changeBody}
        />
      </div>
      <div className="form-group text-right">
        <button
          className="btn btn-outline-secondary px-4"
          disabled={isShoutable || limitCount === props.limitBody}
        >
          Shout
        </button>
      </div>
    </form>
  );
};

ShoutForm.propTypes = {
  action: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  card: PropTypes.string,
  limitBody: PropTypes.number.isRequired,
  changeBody: PropTypes.func.isRequired,
  beforeSubmit: PropTypes.func
};

export default ShoutForm;
